// This file adds everything to init/fini arrays

#include <uniast.h>

/**
 * Initializes UniAST. Called on startup.
 * 
 * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
 * @since 0.1.0
 */
__attribute__ ((__constructor__))
static void
init ()
{
  ast_ensure_types ();
}
