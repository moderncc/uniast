namespace AST
{
  /**
   * Block of statements.
   * 
   * @since 0.1.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Block : Statement
  {
    /**
     * Child statements, executed in order.
     * 
     * @since 0.1.0
     */
    public List<Statement> children;

    /**
     * Creates a new, empty block
     * 
     * @since 0.1.0
     */
    public
    Block ()
    {
      base ();
    }

    /**
     * Creates a new block with given children.
     * 
     * @param children child statements
     * 
     * @since 0.1.0
     */
    public
    Block.with_children (List<Statement> children)
    {
      base ();
      this.children = children.copy ();
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      var array = new Json.Array ();
      foreach (Statement ch in children)
        {
          array.add_object_element (ch.to_json ());
        }

      obj.set_array_member ("children", array);;

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      var array_node = obj.get_member ("children");

      if (array_node == null)
        {
          throw new SerializationError.DESERIALIZE_MISSING_FIELD
            ("Field children not found!");
        }
      if (array_node.get_node_type () != Json.NodeType.ARRAY)
        {
          throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
            ("Expected ARRAY, found "
             + array_node.get_node_type ().to_string ());
        }

      var array = array_node.get_array ();

      for (uint i = 0; i < array.get_length (); i++)
        {
          var node = array.get_element (i);
          if (node.get_node_type () != Json.NodeType.OBJECT)
            {
              throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
              ("Expected OBJECT, found " + node.get_node_type ().to_string ());
            }
          children.append ((Statement) Node.deserialize_json (node.get_object ()));
        }
    }
  }
}
