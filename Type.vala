namespace AST
{
  /**
   * Node which defines a type.
   * 
   * @since 0.1.0
   */
  public class Type : Node
  {
    /**
     * Name of the type.
     * 
     * @since 0.1.0
     */
    public string name;

    /**
     * Creates a new, empty Type.
     * 
     * @since 0.1.0
     */
    public
    Type ()
    {
    }

    /**
     * Creates a Type and initializes it.
     * 
     * @param name name of type
     * 
     * @since 0.1.0
     */
    public
    Type.with_name (string name)
    {
      this.name = name;
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      obj.set_string_member ("name", name);

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      name = obj.get_string_member ("name");
    }
  }
}
