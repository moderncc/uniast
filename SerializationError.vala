namespace AST
{
  /**
   * Indicates that an error has occured while serializing/deserializing.
   * 
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   * @since 0.1.0
   */
  public errordomain SerializationError
  {
    /**
     * Indicates that the class in JSON is unknown.
     * 
     * @since 0.1.0
     */
    DESERIALIZE_UNKNOWN_CLASS,

    /**
     * Indicates that an object was asked to read serialized data of
     * incompatible class.
     * 
     * @since 0.1.0
     */
    DESERIALIZE_WRONG_CLASS_FROM_JSON,

    /**
     * Indicates that class name is missing in JSON.
     * 
     * @since 0.1.0
     */
    DESERIALIZE_MISSING_CLASS,

    /**
     * Indicates that a field is missing in JSON.
     * 
     * @since 0.1.0
     */
    DESERIALIZE_MISSING_FIELD,

    /**
     * Indicates that a class is not a subclass of #AST.Node.
     * 
     * @since 0.1.0
     */
    DESERIALIZE_UNSUPPORTED_CLASS,

    /**
     * Indicates that a field is of wrong type in JSON.
     * 
     * @since 0.1.0
     */
    DESERIALIZE_FIELD_WRONG_TYPE
  }
}
