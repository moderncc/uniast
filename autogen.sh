#!/bin/sh

set -e
set -x

build-aux/gitlog-to-changelog > ChangeLog
autoreconf --force --verbose --install
