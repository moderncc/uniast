namespace AST
{
  /**
   * Set by #ensure_types when the types are ensured. Used as a once-lock.
   * 
   * @since 0.1.0
   */
  private bool types_ensured = false;

  /**
   * Ensures all UniAST types are initialized. Called automatically by the
   * startup routine.
   * 
   * @since 0.1.0
   */
  public void
  ensure_types ()
  {
    if (types_ensured)
      return;
    types_ensured = true;

    typeof (Block).ensure ();
    typeof (Node).ensure ();
    typeof (ScalarType).ensure ();
    typeof (ScalarValue).ensure ();
    typeof (Size).ensure ();
    typeof (Statement).ensure ();
    typeof (Type).ensure ();
    typeof (Typedef).ensure ();
    typeof (Tree).ensure ();
    typeof (Value).ensure ();
  }

  /**
   * Very abstract node, does not define very much itself.
   * 
   * @since 0.1.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Node : Object
  {
    /**
     * The file this node is located in, or null if not known. If this node is
     * scattered over multiple files, for example if tricking using the
     * preprocessor, this means the start location of the first character of
     * the node source code.
     * 
     * @since 0.1.0
     */
    public string? file = null;

    /**
     * The line this node is located in, or -1, if not known. If this node is
     * scattered over multiple lines, this means the start line.
     * 
     * @since 0.1.0
     */
    public ssize_t line = -1;

    /**
     * The column this node starts on, or -1, if not known.
     * 
     * @since 0.1.0
     */
    public ssize_t column = -1;

    /**
     * Constructor. As this class is empty, its constructor is also empty.
     * 
     * @since 0.1.0
     */
    public
    Node ()
    {
    }

    /**
     * Destructor. It is virtual to support subclassing.
     * 
     * @since 0.1.0
     */
    virtual
    ~Node ()
    {
    }

    /**
     * Serializes this object to JSON.
     * 
     * @return generated JSON object
     * 
     * @since 0.1.0
     */
    public virtual Json.Object
    to_json ()
    {
      var obj = new Json.Object ();

      obj.set_string_member ("class", GLib.Type.from_instance (this).name ());
      if (file != null)
        {
          obj.set_string_member ("file", file);
        }
      if (line >= 0)
        {
          obj.set_int_member ("line", line);
        }
      if (column >= 0)
        {
          obj.set_int_member ("column", column);
        }

      return obj;
    }

    /**
     * Reads serialized information from JSON.
     * 
     * @param obj serialized data
     * 
     * @throws SerializationError when some stuff is wrong in JSON
     */
    public virtual void
    from_json (Json.Object obj) throws SerializationError
    {
      var class_node = obj.get_member ("class");

      if (class_node == null)
        {
          throw new SerializationError.DESERIALIZE_MISSING_CLASS
            ("Class not specified!");
        }
      if (class_node.get_node_type () != Json.NodeType.VALUE)
        {
          throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
            ("Expected STRING, found "
             + class_node.get_node_type ().to_string ());
        }

      var json_type = GLib.Type.from_name (class_node.get_string ());
      var expected_type = GLib.Type.from_instance (this);

      if (!json_type.is_a (expected_type))
        {
          throw new SerializationError.DESERIALIZE_WRONG_CLASS_FROM_JSON
            ("Expected class " + expected_type.name ()
             + ", found " + json_type.name ());
        }
    }

    /**
     * Constructs and deserializes an object from JSON. To work properly, one
     * must make sure the type is ensured, so that it is known at the runtime.
     * 
     * @param obj serialized data
     * @return deserialized Node
     * 
     * @throws SerializationError when some stuff is wrong in JSON
     * @since 0.1.0
     */
    public static Node
    deserialize_json (Json.Object obj) throws SerializationError
    {
      ensure_types ();

      var name_node = obj.get_member ("class");

      if (name_node == null)
        {
          throw new SerializationError.DESERIALIZE_MISSING_CLASS
            ("Class not specified!");
        }
      if (name_node.get_node_type () != Json.NodeType.VALUE)
        {
          throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
            ("Expected STRING, found "
             + name_node.get_node_type ().to_string ());
        }

      var type = GLib.Type.from_name (name_node.get_string ());

      type.ensure ();

      if (type == GLib.Type.INVALID)
        {
          throw new SerializationError.DESERIALIZE_UNKNOWN_CLASS
            ("Unknown class: " + name_node.get_string ());
        }

      if (!type.is_a (typeof (Node)))
        {
          throw new SerializationError.DESERIALIZE_UNSUPPORTED_CLASS
            ("Class " + name_node.get_string () + " is not an "
             + typeof (Node).name ()
             + ", it is not a subject for UniAST serialization");
        }

      Node n = Object.new (type) as Node;

      n.from_json (obj);

      return n;
    }
  }
}
