namespace AST
{
  /**
   * Scalar type - just a number, has arithmetical operators and stuff.
   * 
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   * @since 0.1.0
   */
  public class ScalarType : Type
  {
    /**
     * Size of this type. Generally, scalar types have sizes of powers of two,
     * expressed in bytes, but there are exceptions, so no assumptions should
     * be made.
     * 
     * @since 0.1.0
     */
    public Size size;

    /**
     * If set, this type is a signed type, so that ``(0 - 1)`` is ``-1``,
     * otherwise ``(0 - 1)`` evaluates to maximal positive value of this type.
     * 
     * @since 0.1.0
     */
    public bool is_signed;

    /**
     * Creates a new ScalarType with no information provided.
     * 
     * @since 0.1.0
     */
    public
    ScalarType ()
    {
      base ();
    }

    /**
     * Creates a new ScalarType and initializes it.
     * 
     * @param name name of type
     * @param size size of type
     * @param is_signed whether this is a signed type or not
     * 
     * @since 0.1.0
     */
    public
    ScalarType.with_info (string name, Size size, bool is_signed)
    {
      base.with_name (name);

      this.size = size;
      this.is_signed = is_signed;
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();;

      obj.set_object_member ("size", size.to_json ());
      obj.set_boolean_member ("signed", is_signed);

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      size = new Size ();
      size.from_json (obj.get_object_member ("size"));

      is_signed = obj.get_boolean_member ("signed");
    }
  }
}
