namespace AST
{
  /**
   * A tree should be the root element of each AST.
   *
   * @since 0.1.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Tree : Node
  {
    /**
     * Child nodes.
     * 
     * @since 0.1.0
     */
    public List<Node> children;

    /**
     * Creates a new, empty Tree.
     * 
     * @since 0.1.0
     */
    public
    Tree ()
    {
      base ();
    }

    /**
     * Creates a new Tree and initializes it with children.
     * 
     * @param children child nodes
     * 
     * @since 0.1.0
     */
    public
    Tree.with_children (List<Node> children)
    {
      base ();
      this.children = children.copy ();
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      var array = new Json.Array ();
      foreach (Node ch in children)
        {
          array.add_object_element (ch.to_json ());
        }

      obj.set_array_member ("children", array);;

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      var array_node = obj.get_member ("children");

      if (array_node == null)
        {
          throw new SerializationError.DESERIALIZE_MISSING_FIELD
            ("Field children not found!");
        }
      if (array_node.get_node_type () != Json.NodeType.ARRAY)
        {
          throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
            ("Expected ARRAY, found "
             + array_node.get_node_type ().to_string ());
        }

      var array = array_node.get_array ();

      for (uint i = 0; i < array.get_length (); i++)
        {
          var node = array.get_element (i);
          if (node.get_node_type () != Json.NodeType.OBJECT)
            {
              throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
              ("Expected OBJECT, found " + node.get_node_type ().to_string ());
            }
          children.append (Node.deserialize_json (node.get_object ()));
        }
    }
  }
}
