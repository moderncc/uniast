namespace AST
{
  /**
   * A node, which can be called.
   * 
   * @since 0.1.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Statement : Node
  {
    /**
     * Creates a new empty Statement.
     * 
     * @since 0.1.0
     */
    public
    Statement ()
    {
    }
  }
}
