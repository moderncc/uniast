namespace AST
{
  int
  main (string[] args)
  {
    var p = new Json.Parser ();

    try
      {
        p.load_from_file ("dump.json");
      }
    catch (Error err)
      {
        stdout.printf ("[%04d] %s\n", err.code, err.message);
      }
    
    var b = Node.deserialize_json (p.get_root ().get_object ());

    var gen = new Json.Generator ();

    gen.root = new Json.Node.alloc ().init_object (b.to_json ());
    gen.indent = 2;
    gen.indent_char = ' ';
    gen.pretty = true;

    try
      {
        gen.to_file ("dump2.json");
      }
    catch (Error err)
      {
        stdout.printf ("[%04d] %s\n", err.code, err.message);
        return 1;
      }

    return 0;
  }
}
