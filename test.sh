#!/bin/sh

set -ex

rm -f dump.json dump2.json

./example-serialize
./example-reserialize

if [ `(set +e; diff dump.json dump2.json; echo $?)` = 127 ]
then
  DIFF=cmp
else
  DIFF=diff
fi

${DIFF} dump.json dump2.json
