namespace AST
{
  int
  main (string[] args)
  {
    var nodes = new List<Node> ();
    nodes.append (new ScalarType.with_info ("char", new Size.from_bytes (1),
                                            true));
    // TODO: AST node for C structs, C++ classes and ObjC classes
    nodes.append (new Type.with_name ("__gnu_cxx::stdio_filebuf"));
    nodes.append (new Typedef.full ("int8_t", "char"));
    nodes.append (new Node ());
    nodes.append (new ScalarValue.full ("int8_t", { 0xCA }));

    var t = new Tree.with_children (nodes);

    var gen = new Json.Generator ();

    gen.root = new Json.Node.alloc ().init_object (t.to_json ());
    gen.indent = 2;
    gen.indent_char = ' ';
    gen.pretty = true;

    try
      {
        gen.to_file ("dump.json");
      }
    catch (Error err)
      {
        stdout.printf ("[%04d] %s\n", err.code, err.message);
        return 1;
      }

    return 0;
  }
}
