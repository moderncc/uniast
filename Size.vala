namespace AST
{
  /**
   * Size of something, expressed either in bytes or in bits.
   * 
   * @since 0.1.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Size : Node
  {
    /**
     * If set, the size is expressed in bits, otherwise in bytes.
     * 
     * @since 0.1.0
     */
    public bool bits;

    /**
     * Value of this object.
     * 
     * @see bits
     * @since 0.1.0
     */
    public size_t size;

    /**
     * Creates a new empty Size object.
     * 
     * @since 0.1.0
     */
    public
    Size ()
    {
    }

    /**
     * Creates and initializes a Size object.
     * 
     * @param size actual size
     * @param bits if set, the size is in bits, in bytes otherwise
     * 
     * @since 0.1.0
     */
    public
    Size.with_size (size_t size, bool bits)
    {
      this.size = size;
      this.bits = bits;
    }
    
    /**
     * Creates and initializes a size expressed in bytes.
     * 
     * @param size number of bytes
     * 
     * @since 0.1.0
     */
    public
    Size.from_bytes (size_t size)
    {
      this.size = size;
      this.bits = false;
    }

    /**
     * Creates and initializes a size expressed in bits.
     * 
     * @param size number of bits
     * 
     * @since 0.1.0
     */
    public
    Size.from_bits (size_t size)
    {
      this.size = size;
      this.bits = true;
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      obj.set_int_member ("size", (int64) size);
      obj.set_boolean_member ("bits", bits);

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      size = (size_t) obj.get_int_member ("size");
      bits = obj.get_boolean_member ("bits");
    }
  }
}
