namespace AST
{
  /**
   * Value of a type.
   * 
   * @since 0.1.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Value : Node
  {
    /**
     * The type this value represents.
     * 
     * @since 0.1.0
     */
    public string type;

    /**
     * Creates a new, empty value.
     * 
     * @since 0.1.0
     */
    public
    Value ()
    {
      base ();
    }

    /**
     * Creates a new value of a type.
     * 
     * @since 0.1.0
     */
    public
    Value.with_type (string type)
    {
      this.type = type;
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      obj.set_string_member ("type", type);

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      type = obj.get_string_member ("type");
    }

    /**
     * Checks whether the value of this node is known at compile time. This is
     * true for a constant or an operation on constants, but false for any
     * method call or inline assembly.
     * 
     * @return true if can be calculated at compile time, false otherwise
     */
    public virtual bool
    compile_time ()
    {
      return false;
    }
  }
}
