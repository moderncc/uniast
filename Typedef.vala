namespace AST
{
  /**
   * Type, which evaluates to other type.
   * 
   * @since 0.2.0
   * @author Jakub Kaszycki <kuba@kaszycki.net.pl>
   */
  public class Typedef : Type
  {
    /**
     * The type this typedef refers to.
     * 
     * @since 0.2.0
     */
    public string target;

    public
    Typedef ()
    {
    }

    public Typedef.full (string name, string target)
    {
      base.with_name (name);
      this.target = target;
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      obj.set_string_member ("target", target);

      return obj;
    }

    public override void
    from_json (Json.Object obj) throws SerializationError
    {
      base.from_json (obj);

      target = obj.get_string_member ("target");
    }
  }
}
