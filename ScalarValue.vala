namespace AST
{
  public class ScalarValue : Value
  {
    public uint8[] bytes;

    public
    ScalarValue ()
    {
    }

    public
    ScalarValue.full (string type, uint8[] bytes)
    {
      base.with_type (type);
      this.bytes = bytes;
    }

    public override Json.Object
    to_json ()
    {
      var obj = base.to_json ();

      var byte_arr = new Json.Array.sized (bytes.length);

      foreach (uint8 x in bytes)
        {
          byte_arr.add_int_element (x);
        }

      obj.set_array_member ("bytes", byte_arr);

      return obj;
    }
    
    public override void
    from_json (Json.Object obj)
    {
      base.from_json (obj);

      var arr = obj.get_member ("bytes");

      var _bytes = new uint8[0];

      if (arr != null && arr.get_node_type () == Json.NodeType.ARRAY)
        {
          var array = arr.get_array ();

          for (uint i = 0; i < array.get_length (); i++)
            {
              var node = array.get_element (i);
              if (node.get_node_type () != Json.NodeType.VALUE)
                {
                  throw new SerializationError.DESERIALIZE_FIELD_WRONG_TYPE
                  ("Expected VALUE, found " + node.get_node_type ().to_string ());
                }
              _bytes += (uint8) node.get_int ();
            }
        }

      bytes = _bytes;
    }

    public override bool
    compile_time ()
    {
      return true;
    }
  }
}
